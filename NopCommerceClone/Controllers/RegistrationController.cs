﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using NopCommerceClone.Data.EntityModel;
using NopCommerceClone.Web.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NopCommerceClone.Web.Controllers
{
    public class RegistrationController : Controller
    {
        private UserRepository _userRepository = new UserRepository();
        private RoleRepository _roleRepository = new RoleRepository(); 
        private readonly ILogger<RegistrationController> _logger;
        public RegistrationController(ILogger<RegistrationController> logger)
        {
            _logger = logger;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(User user)
        {
            user.RegisteredAt = DateTime.Now;
            if(ModelState.IsValid)
            {
                try
                {
                    if (_userRepository.Get(user)==null)
                    {
                        _userRepository.Insert(user);
                        Role role = new Role
                        {
                            UserID = user.ID,
                            RoleValue = "customer"
                        };
                       _roleRepository.Insert(role);
                        return RedirectToAction("Index", "Login");
                    }
                    else
                    {
                        ModelState.AddModelError("dublicate","Username exists , pick another!");
                        return View(user);
                    }
                   
                }
                catch(Exception e)
                {
                    ModelState.AddModelError("dbDown", "Something wrong please try again later");
                    return View(user);
                }
            }
            else
            {
                return View(user);
            }
        }
    }
}
