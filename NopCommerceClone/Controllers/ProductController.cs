﻿using Microsoft.AspNetCore.Mvc;
using NopCommerceClone.Data.EntityModel;
using NopCommerceClone.Web.Auth;
using NopCommerceClone.Web.Repositories;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NopCommerceClone.Web.Models;

namespace NopCommerceClone.Web.Controllers
{
    public class ProductController : Controller
    {
        private ProductRepository _productRepository = new ProductRepository();
        private CatelogRepository _catelogRepository = new CatelogRepository();
        private CatelogViewModel _catelogViewModel = new CatelogViewModel();
        [HttpGet]
        public IActionResult Create()
        {
            if(Authenticator.AuthSuperAdmin(HttpContext.Session.GetString("user")))
            {
                _catelogViewModel.Catelogs = _catelogRepository.GetAll();
                return View(_catelogViewModel);
            }
            else
            {
                TempData["Unauthorized"] = true;
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Product product)
        {
            product.AddedAt = DateTime.Now;
            product.UserID = Convert.ToInt32(HttpContext.Session.GetString("user"));
            _productRepository.Insert(product);
            TempData["Added"] = true;
            return RedirectToAction("Index", "Home");
        }


        [HttpGet]
        public IActionResult Delete(int id)
        {
            if(Authenticator.AuthAdmin(HttpContext.Session.GetString("user")))
            {
                Product product = _productRepository.GetId(id);
                if (product != null)
                {
                    return View(product);
                }
                TempData["InvalidProduct"] = true;
                return RedirectToAction("Index","Home");
            }
            else
            {
                TempData["Unauthorized"] = true;
                return RedirectToAction("Index","Home");
            }
            
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(Product product,int id)
        {
            product = _productRepository.GetId(id);
            _productRepository.Delete(product.ID);
            return RedirectToAction("Index", "Product");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            _productRepository = new ProductRepository();
            var product = _productRepository.GetId(id);
            if(Authenticator.AuthAdmin(HttpContext.Session.GetString("user")))
            {
                if(product!=null)
                {
                    return View(product);
                }
                else
                {
                    TempData["InvalidProduct"]=true;
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                TempData["Unauthorized"] = true;
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Product product)
        {
            if(Authenticator.AuthAdmin(HttpContext.Session.GetString("user")))
            {
                product.UserID = Convert.ToInt32(HttpContext.Session.GetString("user"));
                _productRepository.Update(product);
                return RedirectToAction("Index", "Admin");
            }
            else
            {
                TempData["Unauthorize"] = true;
                return RedirectToAction("Index", "Home");
            }
        }
    }
}
