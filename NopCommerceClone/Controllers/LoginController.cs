﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NopCommerceClone.Data.EntityModel;
using NopCommerceClone.Models;

using NopCommerceClone.Web.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NopCommerceClone.Web.Models;

namespace NopCommerceClone.Web.Controllers
{
    public class LoginController : Controller
    {
        private readonly ILogger<LoginController> _logger;
        private UserRepository _userRepository = new UserRepository();

        public LoginController(ILogger<LoginController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(LoginViewModel loginViewModel)
        {   
            if(ModelState.IsValid)
            {
                User user = new User();
                user.Password = loginViewModel.Password;
                user.Username = loginViewModel.Username;
                
                if (_userRepository.Validate(user))
                {
                    
                    HttpContext.Session.SetString("user", user.ID.ToString());
                    
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewBag.CredError = "Invalid Login Credentials";
                    return View();
                }
            }
            else
            {
                return View(loginViewModel);
            }
        }
    }
}
