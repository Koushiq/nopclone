﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NopCommerceClone.Web.Auth;
using NopCommerceClone.Web.Repositories;
using NopCommerceClone.Models;

namespace NopCommerceClone.Web.Controllers
{
    public class AdminController : Controller
    {
        private ProductRepository _productRepository = new ProductRepository();
        private RoleRepository _roleRepository = new RoleRepository();
        public int AdminId { get; set; }
        public bool Validate()
        {
            AdminId = Convert.ToInt32(HttpContext.Session.GetString("user"));
            bool result = Authenticator.AuthAdmin(HttpContext.Session.GetString("user"));
            return result;
        }

        public IActionResult Index()
        {
            if(!Validate())
            {
                ViewBag.Unauthorized = true;
            }
            else
            {
                var result = _roleRepository.GetAll().Where(s => s.UserID == AdminId).ToList();
                ProductViewModel productViewModel = new ProductViewModel();
                productViewModel.Products = _productRepository.GetAll();
                productViewModel.Roles = result;
                return View(productViewModel);
            }
            return View();
        }
    }

}
