﻿using NopCommerceClone.Data.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NopCommerceClone.Web.Repositories
{
    public class UserRepository:Repository<User>
    {
        public User Get(User user)
        {
            if(!this.Validate(user))
            {
                return null;
            }
            return user;
        }
        public bool Validate(User user)
        {
            User result=  this.GetAll().Where(s => s.Username == user.Username && s.Password == user.Password).FirstOrDefault();
            if (result != null)
            {
                user.ID = result.ID;
                return true;
            }
            else
            {
                return false;
            }
        }
       
    }
}
