﻿using NopCommerceClone.Data.EntityModel;
using NopCommerceClone.Web.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NopCommerceClone.Web.Auth
{
    public class Authenticator
    {
        public static bool AuthAdmin(string sessionValue)
        {
            
            var results = GetRoles(sessionValue);
            return Role("admin",results);
            
        }
        public static bool AuthSuperAdmin(string sessionValue)
        {
            var results = GetRoles(sessionValue);
            return Role("superadmin", results);
        }
        private static List<Role> GetRoles(string sessionValue)
        {
            int id = Convert.ToInt32(sessionValue);
            RoleRepository roleRepository = new RoleRepository();
            var results = roleRepository.GetAll().Where(s => s.UserID == id).ToList();
            return results;
        }
        private static bool Role(string role, List<Role> results)
        {
            foreach (var item in results)
            {
                if (item.RoleValue ==role)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
