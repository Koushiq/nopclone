﻿using NopCommerceClone.Data.EntityModel;

using NopCommerceClone.Web.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NopCommerceClone.Models
{
    public class ProductViewModel
    {
        public List<Product> Products  { get ; set; }
        public List<Catelog> Catelogs { get; set; }
        public List<Role> Roles { get; set; }
    }
}
