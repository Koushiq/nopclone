﻿using NopCommerceClone.Data.EntityModel;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NopCommerceClone.Data
{
    public class DbInitializer
    {
        public static void Initialize(NopCommereceCloneDbContext context)
        {
            context.Database.EnsureCreated();

            

            if (context.Products.Any())
            {
                return;
            }

            #region seeds
            var users = new User[]
            {
                new User
                {
                    
                    Password="1234",
                    RegisteredAt=DateTime.Now,
                    Username="koushiq"
                },
                new User
                {
                    
                    Password="1234",
                    RegisteredAt=DateTime.Now,
                    Username="koushiq1"
                }
            };


            foreach (User u in users)
            {
                context.Users.Add(u);
            }

            context.SaveChanges();

            var roles = new Role[]
            {
                new Role
                {
                    RoleValue="admin",
                    UserID=1
                },
                new Role
                {
                    RoleValue="customer",
                    UserID=1
                }
                ,
                new Role
                {
                    RoleValue="customer",
                    UserID=2
                }
            };

            foreach (Role r in roles)
            {
                context.Roles.Add(r);
            }

            context.SaveChanges();

            var catelogs = new Catelog[]
            {
                new Catelog
                {
                    
                    CatelogName="Electronics",
                    AddedAt=DateTime.Now,
                    UserID=1
                }
            };

            foreach (var c in catelogs)
            {
                context.Catelogs.Add(c);
            }
            context.SaveChanges();


            var products = new Product[]
            {
                new Product
                {
                    Price=10.00,
                    ProductName="Raspberry Pi",
                    AddedAt=DateTime.Now,
                    UserID=1,
                    CatelogID=1,

                },
                new Product
                {
                    Price=100.00,
                    ProductName="LattePanda",
                    AddedAt=DateTime.Now,
                    UserID=2,
                    CatelogID=1,
                },
                new Product
                {
                    Price=22.33,
                    ProductName="Xiaomi",
                    AddedAt=DateTime.Now,
                    UserID=1,
                    CatelogID=1,
                }
            };

            foreach (Product p in products)
            {
                context.Products.Add(p);
            }

            context.SaveChanges();
            #endregion
        }
    }
}
