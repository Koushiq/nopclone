﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NopCommerceClone.Data.EntityModel
{
    public class Role
    {
        public int ID { get; set; }
        [Required]
        public int UserID { get; set; }
        [Required, MaxLength(100)]
        public string RoleValue { get; set; }
        public virtual User User { get; set; }

        

    }
}
