﻿using Microsoft.EntityFrameworkCore;
using NopCommerceClone.Data.EntityModel;

using System;
using System.Collections.Generic;
using System.Text;

namespace NopCommerceClone.Data
{
    public class NopCommereceCloneDbContext : DbContext
    {
        public DbSet<Catelog> Catelogs { get; set; }
       
        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public NopCommereceCloneDbContext(DbContextOptions<NopCommereceCloneDbContext> options)
        : base(options)
        {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Catelog>().ToTable("Catelog");
            modelBuilder.Entity<Product>().ToTable("Product");
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Role>().ToTable("Role");


           
            //fluent api validation!
            modelBuilder.Entity<User>()
            .HasIndex(u => u.Username)
            .IsUnique();

            modelBuilder.Entity<Product>()
                .HasOne(s => s.Catelog)
                .WithMany(s => s.Products)
                .HasForeignKey(s => s.CatelogID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Role>()
                .HasOne(s => s.User)
                .WithMany(s => s.Roles)
                .HasForeignKey(s => s.UserID)
                .OnDelete(DeleteBehavior.Restrict);
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        { 
            optionsBuilder.UseLazyLoadingProxies();
        }

    }
}
