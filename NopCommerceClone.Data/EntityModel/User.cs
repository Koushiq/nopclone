﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NopCommerceClone.Data.EntityModel
{
    public class User
    {

        public int ID { get; set; }
        [Required]

        //[Index(IsUnique =true)]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        [NotMapped]
        [Compare("Password", ErrorMessage = "Confirm password doesn't match, Type again !")]
        public string ConfirmPassword { get; set; }
        public DateTime RegisteredAt { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
    }
}
